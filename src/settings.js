import { parsedQueryString } from "./utils";

export default {
  gridSize: 6,
  numberOfAvailableOptionsToClick: 16,
  setSize: parseInt(parsedQueryString().setSize) || 6, // ?setSize=10 for a set size of 10
  initialTrialWaitDuration: 500,
  betweenActiveCellDisplayDuration: 1000,
  numberOfTrials: parseInt(parsedQueryString().nTrials) || 24, // ?nTrials=10 for 10 trials
  feedback: !(parseInt(parsedQueryString().ifb) === 0) // ?ifb=0 to disable feedback. By default, feedback is on.
};
