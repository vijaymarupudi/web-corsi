/**
 * @param {*[]} permutationOptions
 * @param {number} permutationLength
 * @return {*[]}
 */
export function permutateWithRepetitions(
  permutationOptions,
  permutationLength = permutationOptions.length
) {
  if (permutationLength === 1) {
    return permutationOptions.map(permutationOption => [permutationOption]);
  }

  // Init permutations array.
  const permutations = [];

  // Go through all options and join it to the smaller permutations.
  permutationOptions.forEach(currentOption => {
    const smallerPermutations = permutateWithRepetitions(
      permutationOptions,
      permutationLength - 1
    );

    smallerPermutations.forEach(smallerPermutation => {
      permutations.push([currentOption].concat(smallerPermutation));
    });
  });

  return permutations;
}

export const stopwatch = {
  startTime: 0,
  start() {
    this.startTime = window.performance.now();
  },
  stop() {
    return window.performance.now() - this.startTime;
  }
};

export function parsedQueryString() {
  const queryString = window.location.search
  var query = {};
  var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
  for (var i = 0; i < pairs.length; i++) {
      var pair = pairs[i].split('=');
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
  }
  return query;
}
