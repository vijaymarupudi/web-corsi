import React, { Component } from "react";
import "bulma/bulma.sass";
import styles from "./styles.module.scss";
import settings from "./settings";
import * as _ from "lodash-es";
import PropTypes from "prop-types";
import * as utils from "./utils";
import produce from "immer";

const BASE_CELL = "BASE_CELL";
const INACTIVE_CELL = "INACTIVE_CELL";
const ACTIVE_CELL = "ACTIVE_CELL";

const POSSIBLE_COORDINATES = utils.permutateWithRepetitions(
  _.range(settings.gridSize),
  2
);

function sleep(time) {
  return new Promise(function(resolve) {
    setTimeout(resolve, time);
  });
}

function getClassName(constant) {
  switch (constant) {
    case BASE_CELL:
      return styles.baseCell;
    case INACTIVE_CELL:
      return styles.inactiveClickableCell;
    case ACTIVE_CELL:
      return styles.activeClickableCell;
    default:
      break;
  }
}

function checkTrial(masterCopy, clickedCellInformation) {
  const clickedCoordinates = clickedCellInformation.map(
    item => item.coordinates
  );

  let numberCorrect = 0;

  for (let i = 0; i < settings.setSize; i++) {
    if (_.isEqual(masterCopy[i], clickedCoordinates[i])) {
      numberCorrect = numberCorrect + 1;
    }
  }

  return Math.round((numberCorrect / settings.setSize) * 100);
}

function genEmptyTableArray() {
  return _.times(settings.gridSize, () =>
    _.times(settings.gridSize, () => BASE_CELL)
  );
}

function genTrialTableArray() {
  const tableArray = genEmptyTableArray(); // to be mutated with random values
  const randomCoordinatesWithinTableBounds = _.sampleSize(
    POSSIBLE_COORDINATES,
    settings.numberOfAvailableOptionsToClick
  );
  for (const [td, tr] of randomCoordinatesWithinTableBounds) {
    tableArray[tr][td] = INACTIVE_CELL;
  }
  return tableArray;
}

/**
 * Essentially the opposite of genTrialTableArray().
 * @param {Array} tableArray A table array
 * @returns {Array} The coordinates filled in the table.
 */
function getManipulatableCoordinates(tableArray) {
  const coordinates = [];
  for (let rowIndex = 0; rowIndex < tableArray.length; rowIndex++) {
    for (
      let columnIndex = 0;
      columnIndex < tableArray[rowIndex].length;
      columnIndex++
    ) {
      if (tableArray[rowIndex][columnIndex] === INACTIVE_CELL) {
        coordinates.push([columnIndex, rowIndex]);
      }
    }
  }
  return coordinates;
}

class TableCell extends Component {
  constructor(props) {
    super(props);
    this.unmounted = false;
    this.state = {
      displayedTdStatus: this.props.tdStatus
    };
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  async handleClick() {
    const { tdStatus, clickable, onClick, coordinates } = this.props;

    const isInactiveAndClickable = tdStatus === INACTIVE_CELL && clickable;

    if (isInactiveAndClickable) {
      if (onClick) onClick(coordinates);

      this.setState({ displayedTdStatus: ACTIVE_CELL });
      await sleep(150);
      if (!this.unmounted) {
        this.setState({ displayedTdStatus: this.props.tdStatus });
      }
    }
  }

  render() {
    const { coordinates } = this.props;

    const className = getClassName(this.state.displayedTdStatus);

    return (
      <td
        className={className}
        onMouseDown={() => {
          this.handleClick(coordinates);
        }}
      />
    );
  }
}
function Table({ tableArray, clickable, onClick = () => {} }) {
  return (
    <table>
      <tbody>
        {tableArray.map((rowArray, rowIndex) => (
          <tr key={rowIndex}>
            {rowArray.map((tdStatus, columnIndex) => (
              <TableCell
                key={columnIndex}
                clickable={clickable}
                coordinates={[columnIndex, rowIndex]}
                tdStatus={tdStatus}
                onClick={onClick}
              />
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
}

Table.propTypes = {
  tableArray: PropTypes.array.isRequired,
  clickable: PropTypes.bool.isRequired
};

function Centered({ children }) {
  return (
    <div className="level">
      <div className="level-item">{children}</div>
    </div>
  );
}

function setCoordinateActiveInTableArray(tableArray, coordinates) {
  return produce(tableArray, ds => {
    ds[coordinates[1]][coordinates[0]] = ACTIVE_CELL;
  });
}

class Trial extends Component {
  constructor(props) {
    super(props);
    this.initialTableArray = genTrialTableArray();
    const manipulatableCoordinates = getManipulatableCoordinates(
      this.initialTableArray
    );

    this.completedTrialData = null;

    this.clickedCellInformationArray = [];

    this.trialCoordinatesArray = _.sampleSize(
      manipulatableCoordinates,
      settings.setSize
    );

    this.state = {
      displayedTableArray: this.initialTableArray,
      clickable: false,
      continueButton: false,
      score: null
    };

    this.initializeTrial();
  }

  async initializeTrial() {
    // Initial delay
    await sleep(settings.initialTrialWaitDuration);

    for (const coordinate of this.trialCoordinatesArray) {
      const displayedTableArray = setCoordinateActiveInTableArray(
        this.initialTableArray,
        coordinate
      );

      this.setState({
        displayedTableArray
      });

      await sleep(settings.betweenActiveCellDisplayDuration);
    }

    this.setState(
      {
        displayedTableArray: this.initialTableArray,
        clickable: true
      },
      () => {
        utils.stopwatch.start();
      }
    );
  }

  handleClick(coordinates) {
    this.clickedCellInformationArray.push({
      time: utils.stopwatch.stop(),
      coordinates: coordinates
    });

    if (this.clickedCellInformationArray.length === settings.setSize) {
      // Trial is over here.
      const duration = utils.stopwatch.stop();

      const score = checkTrial(
        this.trialCoordinatesArray,
        this.clickedCellInformationArray
      );

      this.completedTrialData = {
        duration: duration,
        score: score,
        trialCoordinates: this.trialCoordinatesArray,
        clickedCoordinates: this.clickedCellInformationArray,
        initialTableDisplay: this.initialTableArray
      };

      this.setState({
        continueButton: true,
        clickable: false,
        score: score
      });
    }
  }

  render() {
    return (
      <div>
        <div className="level">
          <div className="level-item">
            <Table
              key={this.state.displayedTableArray}
              tableArray={this.state.displayedTableArray}
              clickable={this.state.clickable}
              onClick={coordinates => this.handleClick(coordinates)}
            />
          </div>
        </div>

        {this.state.continueButton && (
          <div className="content">
            {settings.feedback && (
              <Centered>
                <div
                  className={`notification ${
                    this.state.score === 100 ? "is-success" : "is-warning"
                  }`}
                >
                  {this.state.score === 100
                    ? "Correct!"
                    : `Score: ${this.state.score}%`}
                </div>
              </Centered>
            )}
            <Centered>
              <p>
                <button
                  className="button"
                  onClick={() => this.props.onComplete(this.completedTrialData)}
                >
                  Continue
                </button>
              </p>
            </Centered>
          </div>
        )}
      </div>
    );
  }
}

class App extends Component {
  constructor() {
    super();
    this.trialsData = [];
    this.state = {
      trialIndex: 0,
      finished: false
    };
  }

  completeTrialHandler(trialData) {
    console.log(trialData);
    this.trialsData.push(trialData);

    if (this.trialsData.length === settings.numberOfTrials) {
      const navigator = {};
      for (let i in window.navigator) navigator[i] = window.navigator[i];

      const dataToSend = {
        trialsData: this.trialsData,
        settings: settings,
        metadata: {
          resolution: `${window.screen.width} ${window.screen.height}`,
          deviceInfo: navigator
        }
      };

      if (window.opener) {
        window.opener.postMessage(JSON.stringify(dataToSend), "*");
        setTimeout(() => {
          window.close();
        }, 500);
      }

      this.setState({
        finished: true
      });
      return;
    }

    this.setState({
      trialIndex: this.state.trialIndex + 1
    });
  }

  render() {
    if (this.state.finished) {
      return (
        <section className="hero is-success is-fullheight">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">
                Please wait to be redirected back to the experiment.
              </h1>
            </div>
          </div>
        </section>
      );
    }
    return (
      <div className="section container">
        <Trial
          key={this.state.trialIndex}
          onComplete={trialData => this.completeTrialHandler(trialData)}
        />
      </div>
    );
  }
}

export default App;
